Largest=None
Smallest=None
while True:
    user_number = input("Input a number to be added or type done when you are finished \n")
    try:
        if Largest is None:
            Largest=float(user_number)
        if Smallest is None:
            Smallest=float(user_number)
        if Largest<float(user_number):
            Largest=float(user_number)
        if Smallest>float(user_number):
            Smallest=float(user_number)
    except ValueError:
        if str(user_number)=="done":
            break
        else:
            print("sorry, you must write a number or done")
            continue

print("Largest Number = ", Largest)
print("Smallest Number = ", Smallest)