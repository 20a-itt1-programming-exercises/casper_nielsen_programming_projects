Exercises 1.14 - week 37 - Programming

exercise 1: 
	Answer: c)

Exercise 2: 
	Answer: A compiled code that can run directly from the computer's operating system.

Exercise 3:
	Answer: A compiler does not execute the code like an interpreter does.
	Instead, a compiler simply converts the source code into machine code,
	which can be run directly by the operating system as an executable program.
	Interpreters bypass the compilation process and execute the code directly.

Exercise 4:
	Answer: All of them?

Exercise 5: 
	Answer: it is "print" not "primt"

Exercise 6: 
	Answer: Secondary memory

Exercise 7: 
	Answer: d)

exercise 8: 
	Answer: (1/The Brain) (2/Short term memory) (3/long term memory) (4/the nerve system) (5/the tounge or hands regarding gestures) 

exercise 9:
	Answer: Reading-Running-Ruminating-Retreating