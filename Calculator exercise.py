sum=None
def add(usernum1,usernum2):
    use=usernum1+usernum2
    return use

def subtract(usernum1, usernum2):
    use = usernum1 - usernum2
    return use

def divide(usernum1, usernum2):
    use = usernum1 / usernum2
    return use

def multiply(usernum1, usernum2):
    use = usernum1 * usernum2
    return use


while True:
    try:
        usernum1 = input("Input the first number you want to use or type done when you are finished \n")
        if str(usernum1) == "done":
            print("goodbye")
            break
        usernum2 = input("Input the second number you want to use or type done when you are finished \n")
        if str(usernum2) == "done":
            print("goodbye")
            break
        userfun=input("Do you want to add, subtract, divide or multiply? \n")
        marker=None
        if str(userfun)=="done":
                print("goodbye")
                break
        if userfun==str("add"):
            marker="+"
            sum=add(float(usernum1),float(usernum2))
        if userfun==str("subtract"):
            marker = "-"
            sum=subtract(float(usernum1),float(usernum2))
        if userfun==str("divide"):
            marker = "/"
            sum=divide(float(usernum1),float(usernum2))
        if userfun==str("multiply"):
            marker = "*"
            sum=multiply(float(usernum1),float(usernum2))
    except ZeroDivisionError:
        print("cannot divide by zero, please try again")
    except ValueError:
        if str(userfun)=="done":
            print("goodbye")
            break
        else:
            print("only numbers and done is accepted as inputs, please try again")
            continue
    else:
        try:
            print(float(usernum1),marker,float(usernum2),'=',sum)
        except ValueError:
            print("only numbers and done is accepted as inputs, please try again")
            continue
